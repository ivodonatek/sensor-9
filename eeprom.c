/******************************************************************************
*** eeprom.c ******************************************************************
*******************************************************************************
*   Funkce pro ovladani eeprom ATmega8                                        *
*   - funkce pro zapis do eeprom                                              *
*   - funkce pro cteni z eeprom                                               *
******************************************************************************/

#include <avr/eeprom.h>
#include "eeprom.h"

extern unsigned long long total_64;
extern unsigned long long total_plus_64;
extern unsigned long long total_minus_64;
extern unsigned long long total_aux_plus_64;

extern unsigned long TOTAL_64_H;
extern unsigned long TOTAL_64_L;
extern unsigned long TOTAL_PLUS_64_H;
extern unsigned long TOTAL_PLUS_64_L;
extern unsigned long TOTAL_MINUS_64_H;
extern unsigned long TOTAL_MINUS_64_L;
extern unsigned long TOTAL_AUX_PLUS_64_H;
extern unsigned long TOTAL_AUX_PLUS_64_L;

void EEPROM_Write_Long (void *uiAddress, unsigned long ulData)
{
  eeprom_write_block(&ulData,uiAddress,4);
}

void EEPROM_Write_Float (void *uiAddress, float ulData)
{
  eeprom_write_block(&ulData,uiAddress,4);
}

unsigned long EEPROM_Read_Long (void *uiAddress)
{
  unsigned long Result;
  eeprom_read_block(&Result, uiAddress, 4);
  return Result;
}

float EEPROM_Read_Float (void *uiAddress)
{
  float Result;
  eeprom_read_block(&Result, uiAddress, 4);
  return Result;
}

void EEPROM_Write_Total (void)
{
    EEPROM_Write_Long(&TOTAL_64_H, (unsigned long)(total_64>>32));
    EEPROM_Write_Long(&TOTAL_64_L, (unsigned long)(total_64));
}

void EEPROM_Write_TotalPlus (void)
{
    EEPROM_Write_Long(&TOTAL_PLUS_64_H, (unsigned long)(total_plus_64>>32));
    EEPROM_Write_Long(&TOTAL_PLUS_64_L, (unsigned long)(total_plus_64));
}

void EEPROM_Write_TotalMinus (void)
{
    EEPROM_Write_Long(&TOTAL_MINUS_64_H, (unsigned long)(total_minus_64>>32));
    EEPROM_Write_Long(&TOTAL_MINUS_64_L, (unsigned long)(total_minus_64));
}

void EEPROM_Write_TotalAuxPlus (void)
{
    EEPROM_Write_Long(&TOTAL_AUX_PLUS_64_H, (unsigned long)(total_aux_plus_64>>32));
    EEPROM_Write_Long(&TOTAL_AUX_PLUS_64_L, (unsigned long)(total_aux_plus_64));
}
