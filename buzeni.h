/* buzeni.h */
#ifndef __BUZENI_H_
#define __BUZENI_H_
#include "menu_def.h"
extern void buzeni_init(void);
extern void buzeni (void);

extern long adc_result;
extern long actual_AD;
extern long actual_AD_avg;
extern long actual_ad_average_for_zeroflow;
extern long adc_result_EP;
extern long adc_temperature; // hodnota namerene teplotz internim ADC
extern float actual_flow_float;
extern unsigned char digit_filter;
extern long actual_AD_zero1; // nulova hodnota AD mezi buzenima, prvni smer
extern long actual_AD_zero2; // nulova hodnota AD mezi buzenima, druhy smer
#endif
