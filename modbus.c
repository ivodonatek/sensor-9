#include <avr/interrupt.h>
#include <stddef.h>
#include "mb.h"
#include "mbport.h"
#include "menu_def.h"
#include "eeprom.h"
#include "buzeni.h"
#include "math.h"

extern volatile bool bModbusParametersUpdate;
static unsigned long RegisterPass[REG_PASS_SIZE] = {};
bool kontrola_spravnych_dat ( unsigned long *address, unsigned long data );

eMBErrorCode
eMBRegHoldingCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs,
                 eMBRegisterMode eMode )
{
  eMBErrorCode    eStatus = MB_ENOERR;
  unsigned long *pRegister = NULL;
  unsigned long ulRegData = 0;
  int iRegIndex;

  //kontrola adresace longu
  if((usAddress % 2) || (usNRegs % 2))
  {
    eStatus = MB_ENOREG;
  }
  /** PASS **/
  else if( ( usAddress >= REG_PASS_START)&&(usAddress + usNRegs<= REG_PASS_START+ 2*REG_PASS_SIZE))
  {
    iRegIndex = ((usAddress - REG_PASS_START)>>1);
    if ( eMode==MB_REG_READ )
    {
      while( usNRegs > 0 )
      {
        pRegister = (unsigned long*) RegisterTabPass[iRegIndex];
        if(RegisterPass[iRegIndex] == EEPROM_Read_Long( pRegister ))    //kontrola hesla v RAM s EEPROM
        {
          ulRegData = 1;
        }
        else
        {
          ulRegData = 0;
        }
        *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 8 );
        *pucRegBuffer++ = ( unsigned char )( (ulRegData));
        *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 24 );
        *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 16 );
        usNRegs-=2;
        iRegIndex++;
      }
    }
    else
    {
      while (usNRegs>0)
      {
        pRegister = &RegisterPass[iRegIndex++];
        ulRegData = (unsigned long)(*pucRegBuffer++)<<8;
        ulRegData |= (unsigned long)*pucRegBuffer++;
        ulRegData |= (unsigned long)(*pucRegBuffer++)<<24;
        ulRegData |= (unsigned long)(*pucRegBuffer++)<<16;
        *pRegister = ulRegData ;     //zapis pouze do RAM
        usNRegs-=2;
      }
    }
  }
  /** INFO **/
  else if( ( usAddress >= REG_INFO_START)&&(usAddress + usNRegs<= REG_INFO_START+ 2*REG_INFO_SIZE))
  {
    iRegIndex = ((usAddress - REG_INFO_START)>>1);
    if ( eMode==MB_REG_READ )
    {
      while( usNRegs > 0 )
      {
        pRegister = (unsigned long*) RegisterTabInfo[iRegIndex++];
        ulRegData = EEPROM_Read_Long( pRegister );

        if(pRegister == &OK_MIN)
          ulRegData = OK_min;

        else if(pRegister == &ERRO_MIN)
          ulRegData = Error_min;

        *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 8 );
        *pucRegBuffer++ = ( unsigned char )( (ulRegData));
        *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 24 );
        *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 16 );
        usNRegs-=2;
      }
    }
    else    //zapis
    {
      eStatus = MB_ENOREG;
    }
  }
  /** REAL TIME MEASUREMENT **/
  else if( ( usAddress >= REG_REAL_TIME_START)&&(usAddress + usNRegs<= REG_REAL_TIME_START+ 2*REG_REAL_TIME_SIZE))
  {
    iRegIndex = ((usAddress - REG_REAL_TIME_START)>>1);
    if ( eMode==MB_REG_READ )
    {
      while( usNRegs > 0 )
      {
        pRegister = (unsigned long*) RegisterTabRealTime[iRegIndex++];

        if(pRegister == &REAL_FLOW)
          ulRegData = labs((long)(actual_flow_float*1000.0));
        if(pRegister == &REAL_FLOW_SIGN)
          ulRegData = (actual_flow_float >= 0) ? 0 : 1;
        if(pRegister == &REAL_TOTAL_DIG)
          ulRegData = (unsigned long)(total_64/1000000);
        if(pRegister == &REAL_TOTAL_DEC)
          ulRegData = (unsigned long)(total_64%1000000/1000);
        if(pRegister == &REAL_TOTAL_PLUS_DIG)
          ulRegData = (unsigned long)(total_plus_64/1000000);
        if(pRegister == &REAL_TOTAL_PLUS_DEC)
          ulRegData = (unsigned long)(total_plus_64%1000000/1000);
        if(pRegister == &REAL_TOTAL_MINUS_DIG)
          ulRegData = (unsigned long)(total_minus_64/1000000);
        if(pRegister == &REAL_TOTAL_MINUS_DEC)
          ulRegData = (unsigned long)(total_minus_64%1000000/1000);
        if(pRegister == &REAL_TOTAL_AUX_DIG)
          ulRegData = (unsigned long)(total_aux_plus_64/1000000);
        if(pRegister == &REAL_TOTAL_AUX_DEC)
          ulRegData = (unsigned long)(total_aux_plus_64%1000000/1000);
        if(pRegister == &REAL_ERROR_CODE)
          ulRegData = actual_error;
        if(pRegister == &ACTUAL_ADDATA)
          ulRegData = actual_AD ;
        if(pRegister == &ACTUAL_ADDATAAVG)
          ulRegData = actual_AD_avg;
        if(pRegister == &EMPTY_PIPE_ADC)
          ulRegData = adc_result_EP;
        if(pRegister == &TEPLOTA_AVG)
          ulRegData = adc_temperature;
        if(pRegister == &REAL_TOTAL2_DIG)
          ulRegData = (unsigned long)(total_64/1000);
        if(pRegister == &REAL_TOTAL2_DEC)
          ulRegData = (unsigned long)(total_64%1000);
        if(pRegister == &REAL_TOTAL2_PLUS_DIG)
          ulRegData = (unsigned long)(total_plus_64/1000);
        if(pRegister == &REAL_TOTAL2_PLUS_DEC)
          ulRegData = (unsigned long)(total_plus_64%1000);
        if(pRegister == &REAL_TOTAL2_MINUS_DIG)
          ulRegData = (unsigned long)(total_minus_64/1000);
        if(pRegister == &REAL_TOTAL2_MINUS_DEC)
          ulRegData = (unsigned long)(total_minus_64%1000);
        if(pRegister == &REAL_TOTAL2_AUX_DIG)
          ulRegData = (unsigned long)(total_aux_plus_64/1000);
        if(pRegister == &REAL_TOTAL2_AUX_DEC)
          ulRegData = (unsigned long)(total_aux_plus_64%1000);

        *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 8 );
        *pucRegBuffer++ = ( unsigned char )( (ulRegData));
        *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 24 );
        *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 16 );
        usNRegs-=2;
      }
    }
    else    //zapis
    {
        while (usNRegs>0)
        {
          pRegister = (unsigned long*)RegisterTabRealTime[iRegIndex++];
          ulRegData = (unsigned long)(*pucRegBuffer++)<<8;
          ulRegData |= (unsigned long)*pucRegBuffer++;
          ulRegData |= (unsigned long)(*pucRegBuffer++)<<24;
          ulRegData |= (unsigned long)(*pucRegBuffer++)<<16;

          if (pRegister == &REAL_TOTAL2_DIG)
          {
              total_64 = ulRegData * 1000;
          }
          else if (pRegister == &REAL_TOTAL2_DEC)
          {
              total_64 += ulRegData;
              EEPROM_Write_Total();
          }
          else if (pRegister == &REAL_TOTAL2_PLUS_DIG)
          {
              total_plus_64 = ulRegData * 1000;
          }
          else if (pRegister == &REAL_TOTAL2_PLUS_DEC)
          {
              total_plus_64 += ulRegData;
              EEPROM_Write_TotalPlus();
          }
          else if (pRegister == &REAL_TOTAL2_MINUS_DIG)
          {
              total_minus_64 = ulRegData * 1000;
          }
          else if (pRegister == &REAL_TOTAL2_MINUS_DEC)
          {
              total_minus_64 += ulRegData;
              EEPROM_Write_TotalMinus();
          }
          else if (pRegister == &REAL_TOTAL2_AUX_DIG)
          {
              total_aux_plus_64 = ulRegData * 1000;
          }
          else if (pRegister == &REAL_TOTAL2_AUX_DEC)
          {
              total_aux_plus_64 += ulRegData;
              EEPROM_Write_TotalAuxPlus();
          }

          usNRegs-=2;
        }
    }
  }
  /** REAL TIME MEASUREMENT - FLOAT **/
  else if( ( usAddress >= REG_REAL_TIME_FLOAT_START)&&(usAddress + usNRegs<= REG_REAL_TIME_FLOAT_START+ 2*REG_REAL_TIME_FLOAT_SIZE))
  {
    iRegIndex = ((usAddress - REG_REAL_TIME_FLOAT_START)>>1);
    if ( eMode==MB_REG_READ )
    {
      while( usNRegs > 0 )
      {
        pRegister = (unsigned long*) RegisterTabRealTimeFloat[iRegIndex++];

        if(pRegister == &REAL_FLOW_FLOAT)
          ulRegData = FloatToUlong(actual_flow_float);
        if(pRegister == &REAL_TOTAL_FLOAT)
          ulRegData = FloatToUlong(total_64/1000000.0);
        if(pRegister == &REAL_TOTAL_PLUS_FLOAT)
          ulRegData = FloatToUlong(total_plus_64/1000000.0);
        if(pRegister == &REAL_TOTAL_MINUS_FLOAT)
          ulRegData = FloatToUlong(total_minus_64/1000000.0);
        if(pRegister == &REAL_TOTAL_AUX_FLOAT)
          ulRegData = FloatToUlong(total_aux_plus_64/1000000.0);

        *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 8 );
        *pucRegBuffer++ = ( unsigned char )( (ulRegData));
        *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 24 );
        *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 16 );
        usNRegs-=2;
      }
    }
    else    //zapis
    {
        eStatus = MB_ENOREG; //Pouze cteni
    }
  }
  /** USER **/
  else if( ( usAddress >= REG_USER_START )&&(usAddress + usNRegs<= REG_USER_START + 2*REG_USER_SIZE ) )
  {
    if (RegisterPass[0]!=EEPROM_Read_Long( &PASSWORD_USER ) )   //kontrola hesla
    {
      eStatus = MB_ENOREG; //Chyba hesla
    }
    else
    {
      iRegIndex = ((usAddress - REG_USER_START)>>1);
      if ( eMode==MB_REG_READ )
      {
        while( usNRegs > 0 )
        {
          pRegister = (unsigned long*) RegisterTabUser[iRegIndex++];
          ulRegData = EEPROM_Read_Long( pRegister );

          *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 8 );
          *pucRegBuffer++ = ( unsigned char )( (ulRegData));
          *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 24 );
          *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 16 );
          usNRegs-=2;
        }
      }
      else    //zapis
      {
        while (usNRegs>0)
        {
          pRegister = (unsigned long*)RegisterTabUser[iRegIndex++];
          ulRegData = (unsigned long)(*pucRegBuffer++)<<8;
          ulRegData |= (unsigned long)*pucRegBuffer++;
          ulRegData |= (unsigned long)(*pucRegBuffer++)<<24;
          ulRegData |= (unsigned long)(*pucRegBuffer++)<<16;

          if ((pRegister == &DELETE_AUX) && (ulRegData)) // vymazani AUX
          {
            total_aux_plus_64 = 0;
            EEPROM_Write_TotalAuxPlus();
            ulRegData = 0;
          }

          if (kontrola_spravnych_dat(pRegister, ulRegData))   // zkontroluje jestli jsou vtupni data spravna
            EEPROM_Write_Long( pRegister, ulRegData );
          else                                                // vstupni data nejsou spravna - jsou mimo rozsah
            eStatus = MB_EINVAL; // illegal argument

          //kontrola zmen parametru MODBUSu
          if((pRegister == &MODBUS_SLAVE_ADDRESS)||(pRegister == &MODBUS_BAUDRATE)||(pRegister == &MODBUS_PARITY))
            bModbusParametersUpdate = true;

          usNRegs-=2;
        }
      }
    }
  }
  /** SERVICE **/
  else if( ( usAddress >= REG_SERVICE_START )&&(usAddress + usNRegs<= REG_SERVICE_START + 2*REG_SERVICE_SIZE ) )
  {
    if (RegisterPass[1]!=EEPROM_Read_Long( &PASSWORD_SERVICE ) )   //kontrola hesla
    {
      eStatus = MB_ENOREG; //Chyba hesla
    }
    else
    {
      iRegIndex = ((usAddress - REG_SERVICE_START)>>1);
      if ( eMode==MB_REG_READ )
      {
        while( usNRegs > 0 )
        {
          pRegister = (unsigned long*) RegisterTabService[iRegIndex++];
          ulRegData = EEPROM_Read_Long( pRegister );

          if(pRegister == &ACTUAL_ADZERO1)
            ulRegData = actual_AD_zero1;
          if(pRegister == &ACTUAL_ADZERO2)
            ulRegData = actual_AD_zero2;
          if(pRegister == &ACTUAL_ADZERO_DIF)
            ulRegData = actual_AD_zero1 - actual_AD_zero2;

          *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 8 );
          *pucRegBuffer++ = ( unsigned char )( (ulRegData));
          *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 24 );
          *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 16 );
          usNRegs-=2;
        }
      }
      else    //zapis
      {
        while (usNRegs>0)
        {
          pRegister = (unsigned long*)RegisterTabService[iRegIndex++];
          ulRegData = (unsigned long)(*pucRegBuffer++)<<8;
          ulRegData |= (unsigned long)*pucRegBuffer++;
          ulRegData |= (unsigned long)(*pucRegBuffer++)<<24;
          ulRegData |= (unsigned long)(*pucRegBuffer++)<<16;

          if ((pRegister == &DELETE_TOTAL_VOLUME) && (ulRegData))
          {
            total_64 = 0;
            EEPROM_Write_Total();
            ulRegData = 0;
          }

          else if ((pRegister == &DELETE_POSITIVE_VOLUME) && (ulRegData))
          {
            total_plus_64 = 0;
            EEPROM_Write_TotalPlus();
            ulRegData = 0;
          }

          else if ((pRegister == &DELETE_NEGATIVE_VOLUME) && (ulRegData))
          {
            total_minus_64 = 0;
            EEPROM_Write_TotalMinus();
            ulRegData = 0;
          }

          else if ((pRegister == &DELETE_OK_MIN) && (ulRegData)) // vymazani OK min
          {
            OK_min = 0;
            ulRegData = 0;
          }
          else if ((pRegister == &DELETE_ERROR_MIN) && (ulRegData)) // vymazani ERROR min
          {
            Error_min = 0;
            ulRegData = 0;
          }

          if (kontrola_spravnych_dat(pRegister, ulRegData))   // zkontroluje jestli jsou vtupni data spravna
            EEPROM_Write_Long( pRegister, ulRegData );
          else                                                // vstupni data nejsou spravna - jsou mimo rozsah
            eStatus = MB_EINVAL; // illegal argument

          usNRegs-=2;
        }
      }
    }
  }
  /** FACTORY **/
  else if( ( usAddress >= REG_FACTORY_START )&&(usAddress + usNRegs<= REG_FACTORY_START + 2*REG_FACTORY_SIZE ) )
  {
    if (RegisterPass[2]!=EEPROM_Read_Long( &PASSWORD_FACTORY ) )   //kontrola hesla
    {
      eStatus = MB_ENOREG; //Chyba hesla
    }
    else
    {
      iRegIndex = ((usAddress - REG_FACTORY_START)>>1);
      if ( eMode==MB_REG_READ )
      {
        while( usNRegs > 0 )
        {
          pRegister = (unsigned long*) RegisterTabFactory[iRegIndex++];

          if (pRegister == & CALIBRATION_POINT_ONE ) // pri zapisu do kalibracnich hodnot se hodnota v eeprom aktualizuje az v hlavni smycce
            ulRegData = sensor_cp1;                // proto hodnoty vycitam z RAM, ktere jsou stale aktualni
          else if (pRegister == &CALIBRATION_POINT_TWO)
            ulRegData = sensor_cp2;
          else if (pRegister == &CALIBRATION_POINT_THREE)
            ulRegData = sensor_cp3;
          else if (pRegister == &CALIBRATION_DATA_ONE)
            ulRegData = sensor_cd1;
          else if (pRegister == &CALIBRATION_DATA_TWO)
            ulRegData = sensor_cd2;
          else if (pRegister == &CALIBRATION_DATA_THREE) // jestli se kalibruje tak do eeprom zapisu pozdeji
            ulRegData = sensor_cd3;

          else
            ulRegData = EEPROM_Read_Long( pRegister );

          *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 8 );
          *pucRegBuffer++ = ( unsigned char )( (ulRegData));
          *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 24 );
          *pucRegBuffer++ = ( unsigned char )( (ulRegData) >> 16 );
          usNRegs-=2;
        }
      }
      else    //zapis
      {
        while (usNRegs>0)
        {
          pRegister = (unsigned long*)RegisterTabFactory[iRegIndex++];
          ulRegData = (unsigned long)(*pucRegBuffer++)<<8;
          ulRegData |= (unsigned long)*pucRegBuffer++;
          ulRegData |= (unsigned long)(*pucRegBuffer++)<<24;
          ulRegData |= (unsigned long)(*pucRegBuffer++)<<16;

          if (pRegister == &ZERO_FLOW)
          {
            if (ulRegData)
            {
              Zero_flow_constant = actual_ad_average_for_zeroflow;
              EEPROM_Write_Long(&ZERO_CONSTANT, actual_ad_average_for_zeroflow);
              ulRegData = 0;
            }
          }
          else if (pRegister == &ZERO_ERASE)
          {
            if (ulRegData)
            {
              Zero_flow_constant = 0;
              EEPROM_Write_Float(&ZERO_CONSTANT, 0);
              ulRegData = 0;
            }
          }
          else if (pRegister == &ZERO_CONSTANT)
          {
            Zero_flow_constant = ulRegData;
          }

          else if (pRegister == &EXCITATION_FREQUENCY)
          {
            ExcitationFrequency=ulRegData;
            EEPROM_Write_Long(&EXCITATION_FREQUENCY,ExcitationFrequency);

            switch( ExcitationFrequency )
            {
              case 0: //3.125
                TCCR1B = (1<<WGM12) | (1<<CS12);
                OCR1A = 18432;
              break;

              case 1: //6.25
                TCCR1B = (1<<WGM12) | (1<<CS11) | (1<<CS10);
                OCR1A = 36864;
              break;

              default:    //defaultne 6.25
                TCCR1B = (1<<WGM12) | (1<<CS11) | (1<<CS10);
                OCR1A = 36864;
              break;
            }
          }
          // zkontroluje jestli jsou vtupni data spravna
          if (kontrola_spravnych_dat(pRegister, ulRegData))
            // jestli se kalibruje tak do eeprom zapisu pozdeji
            if(((pRegister == & CALIBRATION_POINT_ONE ) || (pRegister == &CALIBRATION_POINT_TWO) || (pRegister == &CALIBRATION_POINT_THREE) ||
                (pRegister == &CALIBRATION_DATA_ONE) || (pRegister == &CALIBRATION_DATA_TWO) || (pRegister == &CALIBRATION_DATA_THREE)))
              zmena_kalibrace = 6;
            else
              EEPROM_Write_Long( pRegister, ulRegData );
          else                                                // vstupni data nejsou spravna - jsou mimo rozsah
            eStatus = MB_EINVAL; // illegal argument
          usNRegs-=2;
        }
      }
    }
  }
  else
  /** ERROR **/
  {
    eStatus = MB_ENOREG;
  }
  return eStatus;
}

bool kontrola_spravnych_dat ( unsigned long *address, unsigned long data )
// zkontroluje spravnost zapisovanych dat
// provede akci pokud je potreba pri zapisu nejakou akci udelat napr zmena kontrastu
{
  unsigned long tmp;

  if ( address == &AVERAGE_SAMPLES )
  {
    if ((data > AVERAGE_SAMPLES_MAX) || (data < AVERAGE_SAMPLES_MIN))
      return false;
  }

  if ( address == &MODBUS_SLAVE_ADDRESS )
  {
    if ((data > MODBUS_SLAVE_ADDRESS_MAX) || (data < MODBUS_SLAVE_ADDRESS_MIN))
      return false;
  }

  if ( address == &MODBUS_BAUDRATE )
  {
    if ((data != 9600) &&
        (data != 14400) &&
        (data != 19200) &&
        (data != 38400) &&
        (data != 57600) &&
        (data != 115200))
      return false;
  }

  if ( address == &MODBUS_PARITY )
  {
    if ((data > MODBUS_PARITY_MAX) || (data < MODBUS_PARITY_MIN))
      return false;
  }

  if ( address == &DIGIT_FILTER )
  {
    if ((data > 5) || (data < 0))
      return false;
  }

  if ( address == &CLEAN_POWER )
  {
    if (data == 0)
    {
      cisteni_flag = 0;
      PORTA &= ~(1<<PA5);
    }
    else
    {
      cisteni_flag = 1;
      PORTA |= (1<<PA5);
    }
  }

  if (address == &FLOW_RANGE)
    if ((data > FLOW_RANGE_MAX) || (data < FLOW_RANGE_MIN))
      return false;

  if ( address == &EXCITATION_FREQUENCY )
  {
    if ((data > EXCITATION_FREQUENCY_MAX) || (data < EXCITATION_FREQUENCY_MIN))
      return false;
    else
      ExcitationFrequency = data;
  }

  if (address == &CALIBRATION_POINT_ONE)
  {
    sensor_cp1 = data;
    sensor_cd1 = actual_AD_avg-Zero_flow_constant;
    //EEPROM_Write_Long(&CALIBRATION_DATA_ONE, actual_AD_avg-Zero_flow_constant); //do eeprom zapisuji az v hlavni smycce kvuli rzchle odpovedi na kalibraci
  }

  if (address == &CALIBRATION_POINT_TWO)
  {
    sensor_cp2 = data;
    sensor_cd2 = actual_AD_avg-Zero_flow_constant;
    //EEPROM_Write_Long(&CALIBRATION_DATA_TWO, actual_AD_avg-Zero_flow_constant); //do eeprom zapisuji az v hlavni smycce kvuli rzchle odpovedi na kalibraci
  }

  if (address == &CALIBRATION_POINT_THREE)
  {
    sensor_cp3 = data;
    sensor_cd3 = actual_AD_avg-Zero_flow_constant;
    //EEPROM_Write_Long(&CALIBRATION_DATA_THREE, actual_AD_avg-Zero_flow_constant); //do eeprom zapisuji az v hlavni smycce kvuli rzchle odpovedi na kalibraci
  }

  if (address == &CALIBRATION_DATA_ONE)
    sensor_cd1 = data;
  if (address == &CALIBRATION_DATA_TWO)
    sensor_cd2 = data;
  if (address == &CALIBRATION_DATA_THREE)
    sensor_cd3 = data;
  if (address == &DIAMETER) // automaticke nastaveni flow qn a pulse flow
  {
    if ((data != 10) &&
        (data != 15) &&
        (data != 20) &&
        (data != 25) &&
        (data != 32) &&
        (data != 40) &&
        (data != 50) &&
        (data != 65) &&
        (data != 80) &&
        (data != 100) &&
        (data != 125) &&
        (data != 150) &&
        (data != 200) &&
        (data != 250) &&
        (data != 300) &&
        (data != 350) &&
        (data != 400) &&
        (data != 500) &&
        (data != 600) &&
        (data != 700) &&
        (data != 800) &&
        (data != 900) &&
        (data != 1000))
      return false;
    else
    {
      tmp = 0;
      switch (data)
      {
        case 10 :
          tmp = 800;
          break;
        case 15 :
          tmp = 2000;
          break;
        case 20 :
          tmp = 3200;
          break;
        case 25 :
          tmp = 5000;
          break;
        case 32 :
          tmp = 8000;
          break;
        case 40 :
          tmp = 13000;
          break;
        case 50 :
          tmp = 20000;
          break;
        case 65 :
          tmp = 36000;
          break;
        case 80 :
          tmp = 50000;
          break;
        case 100 :
          tmp = 80000;
          break;
        case 125 :
          tmp = 150000;
          break;
        case 150 :
          tmp = 200000;
          break;
        case 200 :
          tmp = 300000;
          break;
        case 250 :
          tmp = 500000;
          break;
        case 300 :
          tmp = 800000;
          break;
        case 350 :
          tmp = 1000000;
          break;
        case 400 :
          tmp = 1300000;
          break;
        case 500 :
          tmp = 2000000;
          break;
        case 600 :
          tmp = 3000000;
          break;
        case 700 :
          tmp = 4000000;
          break;
        case 800 :
          tmp = 5000000;
          break;
        case 900 :
          tmp = 6000000;
          break;
        case 1000 :
          tmp = 8000000;
          break;
      }

      if (tmp != 0)
        EEPROM_Write_Long(&FLOW_RANGE,tmp);
    }
  }
  // osetreni proti zapisu zaporneho cisla
  if ( ((long)data < 0) &&
      (address != &CALIBRATION_DATA_ONE) &&
      (address != &CALIBRATION_DATA_TWO) &&
      (address != &CALIBRATION_DATA_THREE) &&
      (address != &ZERO_CONSTANT))
    return false;

  return true;
}
