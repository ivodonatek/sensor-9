/****************************************************************************
*** spi.h            ********************************************************
*****************************************************************************
****************************************************************************/

#ifndef _SPI_H
#define _SPI_H

extern void SPI_MasterInit(void);
extern void SPI_MasterTransmit(char cData);
extern unsigned char SPI_MasterRead(void);

#define SPI_DDR  DDRB
#define SPI_PORT  PORTB
#define SPI_SS   (1<<PB4)   //soucasne pripojen na ovladani napajeni dipleje
#define SPI_SCK  (1<<PB7)
#define SPI_MOSI (1<<PB5)
#define SPI_MISO (1<<PB6)

#endif
