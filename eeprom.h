/******************************************************************************
*** eeprom.h ******************************************************************
*******************************************************************************
*   Rozhrani pro eeprom.c                                                     *
******************************************************************************/

#ifndef eeprom
#define eeprom

#if ! defined (EEMEM)
#define EEMEM __attribute__ ((section (".eeprom")))
#endif /* EEMEM */

unsigned long EEPROM_Read_Long (void *uiAddress);
float EEPROM_Read_Float (void *uiAddress);
void EEPROM_Write_Long (void *uiAddress, unsigned long ulData);
void EEPROM_Write_Float (void *uiAddress, float ulData);
void EEPROM_Write_Total (void);
void EEPROM_Write_TotalPlus (void);
void EEPROM_Write_TotalMinus (void);
void EEPROM_Write_TotalAuxPlus (void);

#endif //eeprom
