#include "menu_def.h"

extern unsigned long EEPROM_Read_Long (void *uiAddress);

float Compute_Actual_Flow_From_AD_Value (long actual_AD_average)
{
  float a = 0;
  float b = 0;

  if (EEPROM_Read_Long(&CALIBRATION_POINT_THREE) != 0)
  {
    if ((actual_AD_average > sensor_cd3) && (actual_AD_average < sensor_cd2))
    {
      a  = ((sensor_cp2-sensor_cp3)/1000.0)/(sensor_cd2-sensor_cd3); // vypocet hodnoty a pro rovnici primky
      b  = (sensor_cp2/1000.0)-(sensor_cd2*a);                          // vypocet hodnoty b pro rovnici primky
    }
    else if ((actual_AD_average < sensor_cd3) && (actual_AD_average > sensor_cd1))
    {
      a  = ((sensor_cp3-sensor_cp1)/1000.0)/(sensor_cd3-sensor_cd1); // vypocet hodnoty a pro rovnici primky
      b  = (sensor_cp3/1000.0)-(sensor_cd3*a);                          // vypocet hodnoty b pro rovnici primky
    }
    //kalibrace v zapornem prutoku
    else if ((actual_AD_average < sensor_cd3) && (actual_AD_average > sensor_cd2))
    {
      a  = ((sensor_cp2-sensor_cp3)/1000.0)/(sensor_cd2-sensor_cd3); // vypocet hodnoty a pro rovnici primky
      b  = (sensor_cp2/1000.0)-(sensor_cd2*a);                          // vypocet hodnoty b pro rovnici primky
    }
    else if ((actual_AD_average > sensor_cd3) && (actual_AD_average < sensor_cd1))
    {
      a  = ((sensor_cp3-sensor_cp1)/1000.0)/(sensor_cd3-sensor_cd1); // vypocet hodnoty a pro rovnici primky
      b  = (sensor_cp3/1000.0)-(sensor_cd3*a);                          // vypocet hodnoty b pro rovnici primky
    }
    else
    {
      a  = ((sensor_cp2-sensor_cp1)/1000.0)/(sensor_cd2-sensor_cd1); // vypocet hodnoty a pro rovnici primky
      b  = (sensor_cp2/1000.0)-(sensor_cd2*a);                          // vypocet hodnoty b pro rovnici primky
    }
  }
  else
  {
    a  = ((sensor_cp2-sensor_cp1)/1000.0)/(sensor_cd2-sensor_cd1); // vypocet hodnoty a pro rovnici primky
    b  = (sensor_cp2/1000.0)-(sensor_cd2*a);                          // vypocet hodnoty b pro rovnici primky
  }
  return a*actual_AD_average + b;
}
