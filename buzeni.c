#include <avr/io.h>
#include <util/delay.h>
#include "buzeni.h"
#include "adc.h"
#include <eeprom.h>
#include <math.h>

///private
static volatile unsigned char buzeni_typ = 0; // pomocna pro sude nebo liche buzeni
///public
long adc_result; // zde se scitaji vysledky mereni
long adc_result1; // zde se ulozi prumerna zmerena hodnota 1
long adc_result2; // zde se ulozi prumerna zmerena hodnota 2
long adc_result_EP_0 = 0; // hodnota Empty pipe 0
long adc_result_EP_1 = 0; // hodnota Empty pipe 1
long adc_result_EP = 0; // hodnota Empty pipe 1
long adc_temperature = 0; // hodnota namerene teplotz internim ADC
long past_adc_result = 0; // hodnota rozdilu 1. a 2. buzeni z minuleho buzeni
long actual_adc_result = 0; //hodnota rozdilu 1. a 2. buzeni z aktualniho buzeni
long korekce_prutoku1 = 0;
long korekce_prutoku2 = 0;
long temperature_values[30] = {512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512,512};
long actual_AD = 0;
long actual_AD_avg = 0;
long actual_ad_average_for_zeroflow = 0;
float actual_flow_float = 0.0;
long actual_AD_zero1 = 0; // nulova hodnota AD mezi buzenima, prvni smer
long actual_AD_zero2 = 0; // nulova hodnota AD mezi buzenima, druhy smer

volatile long adc_values [AVERAGE_SAMPLES_MAX]; //0 - 119 adc_values
volatile long adc_average = 0; // hodnota prumerne hodnoty ADC

///define
void buzeni_init(void);
void buzeni (void);
long kelnerAverageSmoother(long *array);

///implementation
void buzeni_init(void)
{
  DDRA |= (1<<PA0) | (1<<PA1);    // vystup pro buzeni
  PORTA &= ~(1<<PA0) & ~(1<<PA1); // vystup pro buzeni
  DDRD |= (1<<PD6);    // vystup pro spinani reference 1,23V na vstupy 1 OPZ
  PORTD &= ~(1<<PD6);  // vystup pro spinani reference 1,23V na vstupy 1 OPZ
  DDRC |= (1<<PC2) | (1<<PC3); // vystup pro detekci emty pipe
  DDRA |= (1<<PA2) | (1<<PA3) | (1<<PA4) | (1<<PA5);  // Invert pro buzeni,clean0,clean1,clean_on
  PORTA |= (1<<PA2); // Invert pro buzeni
  PORTA &= ~(1<<PA3) & ~(1<<PA4) & ~(1<<PA5);//vypnuti cisteni elektrod
}

#define AD_ZERO_SAMPLES 50 // pocet vzorku pro mereni nulove hodnoty AD mezi buzenima

void buzeni (void)
{
  float fTmp;
  unsigned int uiTmp;

  PORTD &= ~(1<<PD6);  // vystup pro spinani reference 1,23V na vstupy 1 OPZ

  if( EEPROM_Read_Long(&EXCITATION_FREQUENCY) ) ///0 - 3.125Hz; 1 - 6.250 Hz
  {
    pocet_buzeni++;
    start_delay--;
  }
  else
  {
    pocet_buzeni += 2;
    start_delay -= 2;
  }
  if( start_delay < 0)
    start_delay = 0;

  unsigned char excitation = EEPROM_Read_Long(&EXCITATION);
  unsigned char demo_mode = EEPROM_Read_Long(&DEMO);
  actual_error &= ~ERR_ADC; // vymazani error ADC

  _delay_ms(20); // cekej na ustaleni napajeni po zapnuti
  (void)eMBPoll();

  ADCSRA |= (1<<ADSC);
  // start conversion internal ADC

  PORTD |= (1<<PD6);  // vystup pro spinani reference 1,23V na vstupy 1 OPZ

  ADC_data_read();// test ADC

  adc_result = 0;
  adc_result_EP_0 = 0;
  adc_result_EP_1 = 0;

  if (buzeni_typ)
  {
    if((PINC&(1<<PC1)) && (demo_mode))
      actual_error |= ERR_OVERLOAD;
    else
      actual_error &= ~ERR_OVERLOAD;

    if(!((actual_error&ERR_ADC) || (!(demo_mode)) || (actual_error&ERR_OVERLOAD)) ) // pokud neni chyba ADC a neni demo
    {
      for (unsigned char i=0; i<8; i++)
      {
        adc_result = 0;
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC |= (1<<PC2); // Empty pipe detection
        adc_result_EP_0 += adc_result>>1; // 1.hodnota EP
        adc_result = 0;
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC &= ~(1<<PC2); // Empty pipe detection
        adc_result_EP_1 += adc_result>>1; // 2.hodnota EP
      }
      adc_result_EP = (adc_result_EP_1) - (adc_result_EP_0);

      (void)eMBPoll();
      /// emty pipe detect
      if ((((adc_result_EP) > (long)(6000*EEPROM_Read_Long(&AIR_CONSTANT))) ||
           ((adc_result_EP) < (long)(-6000*EEPROM_Read_Long(&AIR_CONSTANT)))) &&
           !(EEPROM_Read_Long (&AIR_DETECTOR)))
        actual_error |= ERR_EMPTY_PIPE;
      else
      {
        actual_error &= ~ERR_EMPTY_PIPE;
        if (!(excitation)) // pokud je zapnute buzeni
        {
          PORTA |= (1<<PA0); // neni empty pipe buzeni kanal A = 1
          if (!ExcitationFrequency)
            _delay_ms(50);
          else
            _delay_ms(20);
          if (ACSR & (1<<ACO)) // spatne zapojene civky
          {
            PORTA &= ~(1<<PA0); // ukoncim buzeni protoze jsou spatne pripojene civky
            actual_error |= ERR_EXCITATION; // nastaveni erroru
          }
          else
          {
            actual_error &= ~ERR_EXCITATION; // zruseni error flagu
          }
        }
      }
    }

    adc_result = 0;
    (void)eMBPoll();

    if((PINC&(1<<PC1)) && (demo_mode))
      actual_error |= ERR_OVERLOAD;
    else
      actual_error &= ~ERR_OVERLOAD;

    if(!((actual_error&ERR_ADC) || (!(demo_mode)) || (actual_error&ERR_OVERLOAD)) ) // pokud neni chyba ADC a neni demo
    {
      for (unsigned char i=0; i<8; i++)
      {
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC |= (1<<PC2); // Empty pipe detection
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC &= ~(1<<PC2); // Empty pipe detection
      }
      adc_result1 = (adc_result>>5);
      PORTA &= ~(1<<PA0);
    }

    (void)eMBPoll();

    if (!ExcitationFrequency)
      _delay_ms(50);
    else
      _delay_ms(20);

    // mereni nulove hodnoty AD mezi buzenima, prvni smer
    actual_AD_zero1 = 0;
    for (unsigned char s=0; s<AD_ZERO_SAMPLES; s++)
    {
        actual_AD_zero1 += ADC_data_read();
    }
    actual_AD_zero1 = actual_AD_zero1 / AD_ZERO_SAMPLES;

    (void)eMBPoll();
    adc_result = 0;

    if((PINC&(1<<PC1)) && (demo_mode))
      actual_error |= ERR_OVERLOAD;
    else
      actual_error &= ~ERR_OVERLOAD;

    if(!((actual_error&ERR_ADC) || (!(demo_mode)) || (actual_error&ERR_OVERLOAD)) ) // pokud neni chyba ADC a neni demo
    {
      for (unsigned char i=0; i<8; i++)
      {
        adc_result = 0;
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC |= (1<<PC2); // Empty pipe detection
        adc_result = 0;
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC &= ~(1<<PC2); // Empty pipe detection
      }

      (void)eMBPoll();

      if ((!(actual_error&ERR_EMPTY_PIPE)) && (!(excitation))) // jestli neni empty pipe a je zapnute buzeni tak muzu budit
      {
        PORTA |= (1<<PA1);
        if (!ExcitationFrequency)
          _delay_ms(50);
        else
          _delay_ms(20);
        if (ACSR & (1<<ACO)) // spatne zapojene civky
        {
          PORTA &= ~(1<<PA1); // ukoncim buzeni protoze jsou spatne pripojene civky
          actual_error |= ERR_EXCITATION; // nastaveni error flagu
        }
        else
        {
          actual_error &= ~ERR_EXCITATION; // zruseni error flagu
        }
      }
    }

    (void)eMBPoll();
    adc_result = 0;

    if((PINC&(1<<PC1)) && (demo_mode))
      actual_error |= ERR_OVERLOAD;
    else
      actual_error &= ~ERR_OVERLOAD;

    if(!((actual_error&ERR_ADC) || (!(demo_mode)) || (actual_error&ERR_OVERLOAD)) ) // pokud neni chyba ADC a neni demo
    {
      for (unsigned char i=0; i<8; i++)
      {
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC |= (1<<PC2); // Empty pipe detection
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC &= ~(1<<PC2); // Empty pipe detection
      }
      adc_result2 = (adc_result>>5);
      PORTA &= ~(1<<PA1);
    }
    buzeni_typ = 0;
  }
  else
  {
    if((PINC&(1<<PC1)) && (demo_mode))
      actual_error |= ERR_OVERLOAD;
    else
      actual_error &= ~ERR_OVERLOAD;

    if(!((actual_error&ERR_ADC) || (!(demo_mode)) || (actual_error&ERR_OVERLOAD)) ) // pokud neni chyba ADC a neni demo
    {
      for (unsigned char i=0; i<8; i++)
      {
        adc_result = 0;
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC |= (1<<PC3); // Empty pipe detection
        adc_result_EP_0 += adc_result>>1; // 1.hodnota EP
        adc_result = 0;
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC &= ~(1<<PC3); // Empty pipe detection
        adc_result_EP_1 += adc_result>>1; // 2.hodnota EP.
      }
      adc_result_EP = (adc_result_EP_1) - (adc_result_EP_0);

      (void)eMBPoll();
      /// emty pipe detect
      if ((((adc_result_EP) > (long)(6000*EEPROM_Read_Long(&AIR_CONSTANT))) ||
           ((adc_result_EP) < (long)(-6000*EEPROM_Read_Long(&AIR_CONSTANT)))) &&
           !(EEPROM_Read_Long(&AIR_DETECTOR)))
        actual_error |= ERR_EMPTY_PIPE;
      else
      {
        actual_error &= ~ERR_EMPTY_PIPE;
        if (!(excitation)) // pokud je zapnute buzeni
        {
          PORTA |= (1<<PA1); // neni empty pipe buzeni kanal A = 1
          if (!ExcitationFrequency)
            _delay_ms(50);
          else
            _delay_ms(20);
          if (ACSR & (1<<ACO)) // spatne zapojene civky
          {
            PORTA &= ~(1<<PA1); // ukoncim buzeni protoze jsou spatne pripojene civky
            actual_error |= ERR_EXCITATION; // nastaveni error flagu
          }
          else
          {
            actual_error &= ~ERR_EXCITATION; // zruseni error flagu
          }
        }
      }
    }
    (void)eMBPoll();
    adc_result = 0;

    if((PINC&(1<<PC1)) && (demo_mode))
      actual_error |= ERR_OVERLOAD;
    else
      actual_error &= ~ERR_OVERLOAD;

    // pokud neni chyba ADC a neni demo
    if(!((actual_error&ERR_ADC) || (!(demo_mode)) || (actual_error&ERR_OVERLOAD)) )
    {
      for (unsigned char i=0; i<8; i++)
      {
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC |= (1<<PC3); // Empty pipe detection
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC &= ~(1<<PC3); // Empty pipe detection
      }
      adc_result2 = (adc_result>>5);
      PORTA &= ~(1<<PA1);
    }

    (void)eMBPoll();

    if (!ExcitationFrequency)
      _delay_ms(50);
    else
      _delay_ms(20);

    // mereni nulove hodnoty AD mezi buzenima, druhy smer
    actual_AD_zero2 = 0;
    for (unsigned char s=0; s<AD_ZERO_SAMPLES; s++)
    {
        actual_AD_zero2 += ADC_data_read();
    }
    actual_AD_zero2 = actual_AD_zero2 / AD_ZERO_SAMPLES;

    (void)eMBPoll();
    adc_result = 0;

    if((PINC&(1<<PC1)) && (demo_mode))
      actual_error |= ERR_OVERLOAD;
    else
      actual_error &= ~ERR_OVERLOAD;

    if(!((actual_error&ERR_ADC) || (!(demo_mode)) || (actual_error&ERR_OVERLOAD)) ) // pokud neni chyba ADC a neni demo
    {
      for (unsigned char i=0; i<8; i++)
      {
        adc_result = 0;
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC |= (1<<PC3); // Empty pipe detection
        adc_result = 0;
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC &= ~(1<<PC3); // Empty pipe detection
      }
      if ((!(actual_error&ERR_EMPTY_PIPE)) && (!(excitation))) // jestli neni empty pipe a je zapnute buzeni tak muzu budit
      {
        PORTA |= (1<<PA0);
        if (!ExcitationFrequency)
          _delay_ms(50);
        else
          _delay_ms(20);
        if (ACSR & (1<<ACO)) // spatne zapojene civky
        {
          PORTA &= ~(1<<PA0); // ukoncim buzeni protoze jsou spatne pripojene civky
          actual_error |= ERR_EXCITATION; // nastaveni error flagu
        }
        else
        {
          actual_error &= ~ERR_EXCITATION; // zruseni error flagu
        }
      }
    }

    (void)eMBPoll();
    adc_result = 0;

    if((PINC&(1<<PC1)) && (demo_mode))
      actual_error |= ERR_OVERLOAD;
    else
      actual_error &= ~ERR_OVERLOAD;

    if(!((actual_error&ERR_ADC) || (!(demo_mode)) || (actual_error&ERR_OVERLOAD)) ) // pokud neni chyba ADC a neni demo
    {
      for (unsigned char i=0; i<8; i++)
      {
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC |= (1<<PC3); // Empty pipe detection
        for (unsigned char j=0; j<12; j++)
        {
          adc_result += ADC_data_read();// toto zobrazit na display
        }
        PORTC &= ~(1<<PC3); // Empty pipe detection
      }
      adc_result1 = (adc_result>>5);
      PORTA &= ~(1<<PA0);
    }
    buzeni_typ = 1;
  }

  PORTD &= ~(1<<PD7);  // vystup pro spinani reference 1,23V na vstupy 1 OPZ
  /// vypnuti napajeni
  PORTC &= ~(1<<PC1);

  (void)eMBPoll();

  /// epty pipe alarm flag
  if (EEPROM_Read_Long(&AIR_DETECTOR)) // pokud je vypnuty empty pipe alarm tak smazu flag
    actual_error &= ~ERR_EMPTY_PIPE;

  /// ulozeni minule hodnoty rozdilu 1. a 2. buzeni
  past_adc_result = actual_adc_result;

  if (!(demo_mode))
    actual_adc_result = 0;
  else
    actual_adc_result = (adc_result2 - adc_result1); // ulozeni nejnovejsi hodnoty na pozici 0

  /// ulozeni souctu licheho a sudeho buzeni
  if( (actual_error & ERR_OVERLOAD) || (actual_error & ERR_EXCITATION) || (actual_error & ERR_ADC) || (actual_error & ERR_EMPTY_PIPE))
  {
    adc_values[0]=0;
  }
  else
    adc_values[0] = actual_adc_result + past_adc_result;

  /// posun vsech hodnot souctu licheho a sudeho buzeni v poli pro vypocet prumerneho prutoku
  for (unsigned char i=AVERAGE_SAMPLES_MAX-1; i>0; i--)  // posun hodnot v bufferu prutok
    adc_values[i] = adc_values[i-1];        // zmereny prutok

  /// vypocet prumerne hodnoty podle zadane samples per average
  //adc_average = 0;
  long compute_average = 0;
  actual_ad_average_for_zeroflow = 0;

  unsigned char AverageSamples=((unsigned char)EEPROM_Read_Long(&AVERAGE_SAMPLES));
  for (unsigned char i=0; i < AVERAGE_SAMPLES_MAX; i++)
  {
    if( i<AverageSamples)
    // pocitani prumeru
    compute_average += adc_values[i];
    actual_ad_average_for_zeroflow += adc_values[i];
  }
  compute_average /= AverageSamples;
  actual_ad_average_for_zeroflow /= AVERAGE_SAMPLES_MAX;
  adc_average = compute_average;
  actual_AD = adc_values[0];
  // filtr pocita s jadrem 23 nad vzorky bez prumeru
  if( AverageSamples == 1 && EEPROM_Read_Long(&DIGIT_FILTER) == 1)
  {
    actual_AD = kelnerAverageSmoother((long *)adc_values);
  }
  // filtr pocita z prumeru s jadrem o velikosti AverageSamples
  // pokud je cislo sude, tak se pocita s jadrem o velikosti nejblizsiho licheho cisla
  else if( AverageSamples > 1 && EEPROM_Read_Long(&DIGIT_FILTER) == 1)
  {
    actual_AD = kelnerAverageSmoother((long *)adc_values);
  }
  actual_AD_avg = adc_average;

  /// vypocet aktualniho prutoku m3/h - zero flow
  if (!(demo_mode))
    actual_flow_float = EEPROM_Read_Long(&SIMULATED_FLOW)/1000.0;
  else
  {
    if ((actual_error & ERR_EMPTY_PIPE) || (actual_error & ERR_EXCITATION) || (actual_error & ERR_OVERLOAD) || cisteni_flag)
      actual_flow_float=0;
    else
      actual_flow_float = Compute_Actual_Flow_From_AD_Value(adc_average - Zero_flow_constant);
  }

  // pokud je zapnuto Low Flow CutOff
  fTmp = 0;
  switch (EEPROM_Read_Long(&LOW_FLOW_CUTOFF))
  {
    case 0 :
    {
      // pokud je prutok mensi jak 0,5% FLOW_RANGE tak se prutok nastavi na 0
      fTmp = 0.005;
      break;
    }
    case 1 :
    {
      // pokud je prutok mensi jak 1% FLOW_RANGE tak se prutok nastavi na 0
      fTmp = 0.01;
      break;
    }
    case 2 :
    default:
    {
      // pokud je prutok mensi jak 2% FLOW_RANGE tak se prutok nastavi na 0
      fTmp = 0.02;
      break;
    }
    case 3 :
    {
      // pokud je prutok mensi jak 5% FLOW_RANGE tak se prutok nastavi na 0
      fTmp = 0.05;
      break;
    }
    case 4 :
    {
      // pokud je prutok mensi jak 10% FLOW_RANGE tak se prutok nastavi na 0
      fTmp = 0.1;
      break;
    }
    case 5 : // OFF
    break;
  }

  if (fTmp > 0)
  {
      if (fabs(actual_flow_float) < (EEPROM_Read_Long(&FLOW_RANGE)/1000.0 * fTmp))
        actual_flow_float = 0;
  }

  if (EEPROM_Read_Long(&INVERT_FLOW))
    actual_flow_float = -actual_flow_float;

  /// kontrola start delay;
  if( start_delay > 0)
  {
    actual_flow_float=0;
  }

  if( (fabs(actual_flow_float) > ( EEPROM_Read_Long(&FLOW_RANGE)/1000.0)*4.0))
  {
    actual_flow_float=0;
    actual_error |= ERR_OVERLOAD2;
  }
  else
    actual_error &= ~ERR_OVERLOAD2;


  /// vypocet totalizeru podle frekvence buzeni
  if (!(EEPROM_Read_Long(&MEASUREMENT_STATE)))
  {
    switch (ExcitationFrequency)
    {
      case 0: // 3,125 Hz
        uiTmp = 3200;
      break;
      case 1: // 6,250 Hz
      default:
        //#warning"dodelat excitation frequency 3.125 Hz"
        uiTmp = 1600;
      break;
    }

    total_64 +=  actual_flow_float > 0 ? (unsigned long long)(actual_flow_float * uiTmp / 36.0) : (unsigned long long)(-actual_flow_float * uiTmp / 36.0);
    total_plus_64 += (actual_flow_float > 0) ? (unsigned long long)(actual_flow_float * uiTmp / 36.0) : 0;
    total_minus_64 += (actual_flow_float < 0) ? (unsigned long long)(-actual_flow_float * uiTmp / 36.0) : 0;
    total_aux_plus_64 += (actual_flow_float > 0) ? (unsigned long long)(actual_flow_float * uiTmp / 36.0) : 0;

    ///kontrola jestli nepretekl totaliser
    if (total_64 > 999999999000000)  // omezeni velikeho cisla - kdzy je vice jak 999999999m3, tak se pocitadlo vynuluje
    {
      total_64 = 0;
      EEPROM_Write_Total();
    }
    if (total_plus_64 > 999999999000000)        // omezeni velikeho cisla - kdzy je vice jak 999999999m3, tak se pocitadlo vynuluje
    {
      total_plus_64 = 0;
      EEPROM_Write_TotalPlus();
    }
    if (total_minus_64 > 999999999000000)  // omezeni veli1keho cisla - kdzy je vice jak 999999999m3, tak se pocitadlo vynuluje
    {
      total_minus_64 = 0;
      EEPROM_Write_TotalMinus();
    }
    if (total_aux_plus_64 > 999999999000000)        // omezeni velikeho cisla - kdzy je vice jak 999999999m3, tak se pocitadlo vynuluje
    {
      total_aux_plus_64 = 0;
      EEPROM_Write_TotalAuxPlus();
    }
  }

  if (actual_error)
  {
    error_min_flag = true;
  }
}

SIGNAL (SIG_ADC)
{
  temperature_values[0] = ADCL + (ADCH * 256);
  temperature_values[0] = 3.322*temperature_values[0] - 1484.9;

  /// posun vsech hodnot souctu licheho a sudeho buzeni v poli pro vypocet prumerneho prutoku
  for (unsigned char i=30-1; i>0; i--)  // posun hodnot v bufferu prutok
    temperature_values[i] = temperature_values[i-1];        // zmereny prutok

  /// vypocet prumerne hodnoty podle zadane samples per average
  //adc_average = 0;

  adc_temperature = 0;

  for (unsigned char i=0; i < 30; i++)
  {
    // pocitani prumeru
    adc_temperature += temperature_values[i];
  }
  adc_temperature /= 30;
  if ((adc_temperature > 1800) || (adc_temperature < -1300))
    actual_error |= ERR_TEMPERATURE;
  else
    actual_error &= ~ERR_TEMPERATURE;
}

long kelnerAverageSmoother(long *data)
{
  uint8_t lenght = sizeof(data)/sizeof(data[0]);
  uint8_t index = lenght/2;
  uint8_t mult = 1;
  long s = 0;
  for (uint8_t i = 0; i < lenght; i++)
  {
    s += mult * data[i];
    if (i < index)
      mult += 1;
    if (i >= index)
      mult -= 1;
  }
  uint8_t delitel = pow( index + 1,2);
  s = (int)(round(s/delitel));
  return s;
}
