#ifndef ADC_H_INCLUDED
#define ADC_H_INCLUDED

#define AD_CS_DDR DDRD
#define AD_CS_PORT PORTD
#define AD_CS_PIN (1<<PD4)

extern void ADC_init( void );
extern long ADC_data_read( void );

#endif // ADC_H_INCLUDED
