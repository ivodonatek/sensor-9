#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <avr/sleep.h>
#define RTS_ENABLE (1)
#include "spi.h"
#include "buzeni.h"

typedef struct DateTime
{
  unsigned int msecond;
  unsigned char second;   //enter the current time, date, month, and year
  unsigned char minute;
  unsigned char hour;
  unsigned char day;
  unsigned char month;
  unsigned int year;
}TDateTime;

volatile TDateTime DateTime;

unsigned long sensor_cp1;
unsigned long sensor_cp2;
unsigned long sensor_cp3;
long sensor_cd1;
long sensor_cd2;
long sensor_cd3;
unsigned char zmena_kalibrace = 0;
unsigned char display_sude = 0;
volatile bool buzeni_flag = 0; // pomocna pro provedeni buzeni (0-nebudi, 1-budi)
volatile bool cisteni_flag = 0; // pomocna pro provedeni buzeni (0-necisti, 1-cisti)
long Zero_flow_constant = 0;

unsigned char ExcitationFrequency; // frekvence buzeni
unsigned char digit_filter;

unsigned long actual_error = 0; // kod aktualni chyby (0 - bez chyby)
unsigned long Error_min = 0; // pocitadlo
bool error_min_flag = false; // pokud v aktualni minute byla chyba tak po teto minute inkrementuji error min a ne ok min
unsigned long OK_min = 0; // pocitadlo
volatile unsigned int pocet_buzeni=0;
volatile int start_delay=0;

unsigned long long total_64 = 0;          // total [m3]
unsigned long long total_plus_64 = 0;     // total+ [m3]
unsigned long long total_minus_64 = 0;    // total- [m3]
unsigned long long total_aux_plus_64 = 0; // total_aux_plus [m3]

//unsigned long unix_stamp = 359240110;

/// error codes
#define ERR_EMPTY_PIPE  (1<<0)
#define ERR_EXCITATION  (1<<2)
#define ERR_OVERLOAD    (1<<1)
#define ERR_ADC         (1<<7)
#define ERR_TEMPERATURE (1<<13)
//#define ERR_OVERLOAD2   (1<<24)
#define ERR_OVERLOAD2   (0x1000000)

unsigned long FloatToUlong(float value);

#include "modbus.c"
#include "calibration.c"
#include "buzeni.c"

#define POCITADEL 4 // pocet pocitadel

volatile bool bModbusParametersUpdate = false;
volatile unsigned long DisplayRefreshCounter = 0;
extern volatile bool preruseni_casovace;
//extern volatile TDateTime DateTime;


float Convert_Flow(float in, unsigned char typ);
float Convert_Volume(float in, unsigned char typ);


/*************************************************************************************************
*                               Main                                                             *
**************************************************************************************************/
int main (void)
{
  /// inicializace promennych
  OK_min = EEPROM_Read_Long(&OK_MIN);
  Error_min = EEPROM_Read_Long(&ERRO_MIN);
  digit_filter = EEPROM_Read_Long(&DIGIT_FILTER);

  sensor_cp1 = EEPROM_Read_Long(&CALIBRATION_POINT_ONE);
  sensor_cp2 = EEPROM_Read_Long(&CALIBRATION_POINT_TWO);
  sensor_cp3 = EEPROM_Read_Long(&CALIBRATION_POINT_THREE);
  sensor_cd1 = EEPROM_Read_Long(&CALIBRATION_DATA_ONE);
  sensor_cd2 = EEPROM_Read_Long(&CALIBRATION_DATA_TWO);
  sensor_cd3 = EEPROM_Read_Long(&CALIBRATION_DATA_THREE);

  Zero_flow_constant = EEPROM_Read_Long(&ZERO_CONSTANT);

  ExcitationFrequency = EEPROM_Read_Long(&EXCITATION_FREQUENCY); // inicializece promenne

  total_64 =  (unsigned long long)EEPROM_Read_Long(&TOTAL_64_H) << 32;
  total_64 += EEPROM_Read_Long(&TOTAL_64_L);

  total_plus_64 =  (unsigned long long)EEPROM_Read_Long(&TOTAL_PLUS_64_H) << 32;
  total_plus_64 += EEPROM_Read_Long(&TOTAL_PLUS_64_L);

  total_minus_64 =  (unsigned long long)EEPROM_Read_Long(&TOTAL_MINUS_64_H) << 32;
  total_minus_64 += EEPROM_Read_Long(&TOTAL_MINUS_64_L);

  total_aux_plus_64 =  (unsigned long long)EEPROM_Read_Long(&TOTAL_AUX_PLUS_64_H) << 32;
  total_aux_plus_64 += EEPROM_Read_Long(&TOTAL_AUX_PLUS_64_L);

  start_delay=EEPROM_Read_Long(&START_DELAY)*6.25;    //nacte se pocet sekund a vynasobi se buzenim

  SPI_MasterInit();
  buzeni_init();
  ADC_init();

  ///inicializace interniho ADC
  ADMUX |= (1<<REFS0) | (1<<REFS1) | (1<<MUX0) | (1<<MUX1) | (1<<MUX2);
  // internal reference 2.56V with externa capacitor at AREF pin, ADC7 single ended input
  ADCSRA |= (1<<ADEN) | (1<<ADIE) | (1<<ADPS0)  | (1<<ADPS1)  | (1<<ADPS2) ;
  // AD enable, AD interupt enable

  //buzeni,
  //TCCR1B |= (1<<WGM12) ;// prescaller64

  switch( ExcitationFrequency )
  {
    case 0: //3.125
      TCCR1B = (1<<WGM12) | (1<<CS12);
      OCR1A = 18432;
    break;

    case 1: //6.25
      TCCR1B = (1<<WGM12) | (1<<CS11) | (1<<CS10);
      OCR1A = 36864;
    break;

    default:    //defaultne 6.25
      TCCR1B = (1<<WGM12) | (1<<CS11) | (1<<CS10);
      OCR1A = 36864;
    break;
  }
  TIMSK1 |= (1<<OCIE1A);//TIMSK |= (1<<OCIE1A);//atmega32

  eMBErrorCode    eStatus;
  eStatus = eMBInit( MB_RTU, EEPROM_Read_Long(&MODBUS_SLAVE_ADDRESS), 0, EEPROM_Read_Long(&MODBUS_BAUDRATE), EEPROM_Read_Long(&MODBUS_PARITY ) );
  eStatus = eMBEnable( );

  sei();

  char test=0;

  while (true)
  {
    (void)eMBPoll();
    if (bModbusParametersUpdate)
    {
      for (unsigned char i =0; i<10; i++) //delay 100ms for modbus answer
        _delay_ms(10);

      eMBDisable();
      eMBClose();
      eMBInit( MB_RTU, EEPROM_Read_Long(&MODBUS_SLAVE_ADDRESS), 0, EEPROM_Read_Long(&MODBUS_BAUDRATE), EEPROM_Read_Long(&MODBUS_PARITY ) );
      eMBEnable();
      bModbusParametersUpdate = false;
    }

    if (buzeni_flag) // ma se budit?
    {
      buzeni();
      if (cisteni_flag)
      {
        if (test)
        {
          PORTA |= (1<<PA3);
          PORTA &= ~(1<<PA4);
          test=0;
        }
        else
        {
          PORTA |= (1<<PA4);
          PORTA &= ~(1<<PA3);
          test=1;
        }
      }
      buzeni_flag = 0;
    }

    ///zalohovani
    if ( pocet_buzeni >= 25200)  ///cca 1 hodina - pricita se v buzeni.c
    {
      pocet_buzeni=0;
      EEPROM_Write_Total();
      EEPROM_Write_TotalPlus();
      EEPROM_Write_TotalMinus();
      EEPROM_Write_TotalAuxPlus();

      EEPROM_Write_Float(&OK_MIN,OK_min);
      EEPROM_Write_Float(&ERRO_MIN,Error_min);
    }

    if (zmena_kalibrace) // zmenili se kalibracni konstanty musim je zapsat do eeprom
    {
      if (zmena_kalibrace == 6) EEPROM_Write_Long(&CALIBRATION_POINT_ONE,   sensor_cp1);
      else if (zmena_kalibrace == 5) EEPROM_Write_Long(&CALIBRATION_POINT_TWO,   sensor_cp2);
      else if (zmena_kalibrace == 4) EEPROM_Write_Long(&CALIBRATION_POINT_THREE, sensor_cp3);
      else if (zmena_kalibrace == 3) EEPROM_Write_Long(&CALIBRATION_DATA_ONE,    sensor_cd1);
      else if (zmena_kalibrace == 2) EEPROM_Write_Long(&CALIBRATION_DATA_TWO,    sensor_cd2);
      else if (zmena_kalibrace == 1) EEPROM_Write_Long(&CALIBRATION_DATA_THREE,  sensor_cd3);
      zmena_kalibrace--;
    }
  }
}

SIGNAL (SIG_OUTPUT_COMPARE1A)
{
  buzeni_flag = 1;
}

unsigned long FloatToUlong(float value)
{
    unsigned long result;
    unsigned char i;
    char *pS, *pD;

    pS = (char *)&value;
    pD = (char *)&result;

    for (i = 0; i < 4; i++)
    {
        *pD = *pS;
        pS++;
        pD++;
    }

    return result;
}
