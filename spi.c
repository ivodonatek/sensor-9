/****************************************************************************
***  spi.c  *****************************************************************
*****************************************************************************/
#include <avr/io.h>
#include <avr/version.h>
#include "spi.h"

#if __AVR_LIBC_VERSION__ > 10405UL
  #define SPCR0 SPCR
  #define SPE0 SPE
  #define MSTR0 MSTR
  #define CPOL0 CPOL
  #define CPHA0 CPHA
  #define SPSR0 SPSR
  #define SPI2X0 SPI2X
  #define SPDR0 SPDR
  #define SPSR0 SPSR
  #define SPIF0 SPIF
#endif

void SPI_MasterInit(void)
{
  /* Set MOSI and SCK outputand SS, A0, all others input */
  SPI_DDR |= SPI_SS | SPI_SCK | SPI_MOSI;
  SPI_PORT |= SPI_SS | SPI_SCK | SPI_MOSI;
  /* Enable SPI, Master, set clock rate fck/4, mode 3 */
  SPCR0 = (1<<SPE0)|(1<<MSTR0)|(1<<CPOL0)|(1<<CPHA0);
  SPSR0 = (1<<SPI2X0); // double SPI speed Bit fck/2
}

void SPI_MasterTransmit(char cData)
{
  /* Start transmission */
  SPDR0 = cData;
  /* Wait for transmission complete */
  while(!(SPSR0 & (1<<SPIF0)));
}

unsigned char SPI_MasterRead(void)
{
  /* Start transmission */
  SPDR0 = 0xff;
  /* Wait for transmission complete */
  while(!(SPSR0 & (1<<SPIF0)));
  return SPDR;
}
