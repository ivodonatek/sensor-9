#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>
#include "adc.h"
#include "spi.h"

#if __AVR_LIBC_VERSION__ > 10405UL
  #define SPCR0 SPCR
  #define SPE0 SPE
  #define MSTR0 MSTR
  #define CPOL0 CPOL
  #define CPHA0 CPHA
  #define SPSR0 SPSR
  #define SPI2X0 SPI2X
  #define SPDR0 SPDR
  #define SPSR0 SPSR
  #define SPIF0 SPIF
#endif

#define SCLKPORT PORTB
#define SCLKDDR  DDRB
#define SCLK     PB7

#define DOUTPIN  PINB
#define DOUTPORT PORTB
#define DOUTDDR  DDRB
#define DOUT     PB6
#define ERR_ADC  (1<<7)

///private
static volatile unsigned char ADC_mode = 0;

///public

///define
extern unsigned long actual_error;

//static bool ADC_data_ready (void);
void ADC_init( void );


///implementation
void ADC_init( void )
{
  AD_CS_DDR |= AD_CS_PIN; // AD CS to out
  AD_CS_PORT |= AD_CS_PIN; // AD CS = 1

  DDRD |= (1<<PD7);    //fast clk pro prevodnik
  PORTD|=(1<<PD7);

  TCCR2A = (1<<WGM21) | (1<<COM2A0);//| (1<<WGM20)//TCCR2 = (1<<WGM21)  | (1<<COM20) | (1<<CS20);//| (1<<WGM20)//atmega32
  TCCR2B = (1<<CS20);
  OCR2A = 0;	//7,373 Mhz//OCR2 = 0;	//7,373 Mhz//atmega32

  //SCLKDDR |= (1<<SCLK); // SCLK for ADS1251
  //DOUTDDR &= ~(1<<DOUT); // DOUT for ADS1251

  //DOUTPORT |= (1<<DOUT); // PullUp for DOUT
  //SCLKPORT &= ~(1<<SCLK); // SCLK to LOW
}


/*
static bool ADC_data_ready (void)
{
    if ((PINB & (1<<PB6)) && (!ADC_mode)) // je DOUT = 1 a ADC = 0 ?
        ADC_mode = 1;

    if (!(PINB & (1<<PB6)) && (ADC_mode == 1)) // je DOUT = 0 a ADC = 1 ?
        ADC_mode = 2;

    if ((PINB & (1<<PB6)) && (ADC_mode == 2)) // je DOUT = 1 a ADC = 2 ?
    {
        ADC_mode = 0;
        return true;
    }
    return false;
}
*/

long ADC_data_read( void )
{
  //return 1;//odkomentovat
  if(!(actual_error & ERR_ADC)) // pokud neni chyba ADC
  {
    long Result = 0;
    unsigned char Timeout = 255;//60
    //unsigned char zSPCR0 = SPCR0; // zaloha nastaveni SPI
    SPCR0 &= ~(1<<CPOL) & ~(1<<CPHA) & ~(1<<SPE); // nastaveni SPI mode 0, SPI Disable
    SPCR0 |= (1<<SPE); // SPI Enable
    //AD_CS_PORT &= ~AD_CS_PIN; // AD CS = 0

    while((!(PINB & (1<<PB6)))) // cekej na 1
    {
      if (!Timeout) break;
      Timeout--;
    }

    while((PINB & (1<<PB6)))  // cekej na 0
    {
      if (!Timeout) break;
      Timeout--;
    }
    Timeout=255;
    while((!(PINB & (1<<PB6)))) // cekej na 1
    {
      if (!Timeout) break;
      Timeout--;
    }

    if(!Timeout)
    {
      // AD_CS_PORT |= AD_CS_PIN; // AD CS = 1
      // SPCR0 = zSPCR0; //obnova zalohy SPCR0
      actual_error |= ERR_ADC; // nastaveni error ADC

      _delay_ms(5);
      return 0; // vratim chybu adc
    }
    else
    {
      Result =  SPI_MasterRead();
      Result <<= 8;
      Result += SPI_MasterRead();
      Result <<= 8;
      Result += SPI_MasterRead();
    }

    if (Result > 0x7FFFFF) // pokud ma byt vysledek zaporne cislo
    {
      Result = 0xFFFFFF - Result;
      Result *= -1;
    }
    return Result;
  }
  else
    return 0;
}
