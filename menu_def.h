#ifndef __MENU_DEF_H__
#define __MENU_DEF_H__

#if ! defined (EEMEM)
#define EEMEM __attribute__ ((section (".eeprom")))
#endif /* EEMEM */

#include"mbport.h"

#define FW_VERSION  3010

/**polozky menu, ulozen vsechny za sebou    **/
unsigned long UNKNOWN           EEMEM  = 0;
unsigned long UNKNOWN2          EEMEM  = 0;

unsigned long PASSWORD_USER EEMEM =  1111 ;
unsigned long PASSWORD_SERVICE EEMEM =  5831 ;
unsigned long PASSWORD_FACTORY EEMEM =  7367 ;
unsigned long UNIT_NO EEMEM  =  38200040 ;
unsigned long ERRO_MIN EEMEM =  0 ;
unsigned long OK_MIN EEMEM =  0 ;
unsigned long DIAMETER EEMEM =  0 ;
unsigned long FIRM_WARE EEMEM =  FW_VERSION ;
unsigned long FLOW_RANGE EEMEM =  3600 ;
unsigned long MEASUREMENT_STATE EEMEM =  1 ;
unsigned long AIR_DETECTOR   EEMEM =  1 ;
unsigned long AIR_CONSTANT EEMEM =  188 ;

unsigned long AVERAGE_SAMPLES EEMEM =  15 ;
unsigned long LOW_FLOW_CUTOFF EEMEM =  3 ;
unsigned long INVERT_FLOW EEMEM =  0 ;
unsigned long CLEAN_POWER EEMEM = 0;
unsigned long DELETE_AUX EEMEM =  0 ;
unsigned long MODBUS_SLAVE_ADDRESS EEMEM =  1 ;
unsigned long MODBUS_BAUDRATE EEMEM =  9600 ;
unsigned long MODBUS_PARITY EEMEM = (eMBParity)MB_PAR_NONE;
unsigned long START_DELAY EEMEM = 15;
unsigned long DIGIT_FILTER EEMEM = 0;
unsigned long DELETE_OK_MIN EEMEM = 0;
unsigned long DELETE_ERROR_MIN EEMEM = 0;
unsigned long DELETE_TOTAL_VOLUME EEMEM = 0;
unsigned long DELETE_NEGATIVE_VOLUME EEMEM =  0 ;
unsigned long DELETE_POSITIVE_VOLUME EEMEM =  0 ;
unsigned long DEMO EEMEM =  1 ;
unsigned long SIMULATED_FLOW EEMEM =  3600 ;
unsigned long CALIBRATION_POINT_ONE EEMEM =  0 ;
unsigned long CALIBRATION_DATA_ONE EEMEM =  0 ;
unsigned long CALIBRATION_POINT_TWO EEMEM =  100 ; // 10 m3
unsigned long CALIBRATION_DATA_TWO EEMEM =  100000 ;
unsigned long CALIBRATION_POINT_THREE EEMEM =  0 ;
unsigned long CALIBRATION_DATA_THREE EEMEM =  0 ;
unsigned long ZERO_FLOW EEMEM =  0 ;
unsigned long ZERO_CONSTANT EEMEM =  0 ;
unsigned long ZERO_ERASE EEMEM =  0 ;
unsigned long EXCITATION_FREQUENCY EEMEM =  1 ;
unsigned long EXCITATION EEMEM =  0 ;

unsigned long ACTUAL_ADDATA = 0;
unsigned long ACTUAL_ADDATAAVG = 0;
unsigned long EMPTY_PIPE_ADC=0;
unsigned long TEPLOTA_AVG=0;

// AD zero measurement
unsigned long ACTUAL_ADZERO1 = 0;
unsigned long ACTUAL_ADZERO2 = 0;
unsigned long ACTUAL_ADZERO_DIF = 0;

/// real time measurement
unsigned long REAL_FLOW = 0;
unsigned long REAL_FLOW_SIGN = 0;
unsigned long REAL_TOTAL_DIG = 0;
unsigned long REAL_TOTAL_DEC = 0;
unsigned long REAL_TOTAL_PLUS_DIG = 0;
unsigned long REAL_TOTAL_PLUS_DEC = 0;
unsigned long REAL_TOTAL_MINUS_DIG = 0;
unsigned long REAL_TOTAL_MINUS_DEC = 0;
unsigned long REAL_TOTAL_AUX_DIG = 0;
unsigned long REAL_TOTAL_AUX_DEC = 0;
unsigned long REAL_ERROR_CODE = 0;

/// real time measurement - accurate totals
unsigned long REAL_TOTAL2_DIG = 0;
unsigned long REAL_TOTAL2_DEC = 0;
unsigned long REAL_TOTAL2_PLUS_DIG = 0;
unsigned long REAL_TOTAL2_PLUS_DEC = 0;
unsigned long REAL_TOTAL2_MINUS_DIG = 0;
unsigned long REAL_TOTAL2_MINUS_DEC = 0;
unsigned long REAL_TOTAL2_AUX_DIG = 0;
unsigned long REAL_TOTAL2_AUX_DEC = 0;

/// real time measurement - float
unsigned long REAL_FLOW_FLOAT = 0;
unsigned long REAL_TOTAL_FLOAT = 0;
unsigned long REAL_TOTAL_PLUS_FLOAT = 0;
unsigned long REAL_TOTAL_MINUS_FLOAT = 0;
unsigned long REAL_TOTAL_AUX_FLOAT = 0;

/// pro ukladani totalizeru
//float TOTAL EEMEM =  0 ;
//float TOTAL_PLUS EEMEM = 0 ;
//float TOTAL_MINUS EEMEM = 0 ;
//float TOTAL_AUX_PLUS EEMEM = 0 ;
unsigned long TOTAL_64_H EEMEM =  0 ;
unsigned long TOTAL_64_L EEMEM =  0 ;
unsigned long TOTAL_PLUS_64_H EEMEM = 0 ;
unsigned long TOTAL_PLUS_64_L EEMEM = 0 ;
unsigned long TOTAL_MINUS_64_H EEMEM = 0 ;
unsigned long TOTAL_MINUS_64_L EEMEM = 0 ;
unsigned long TOTAL_AUX_PLUS_64_H EEMEM = 0 ;
unsigned long TOTAL_AUX_PLUS_64_L EEMEM = 0 ;

// kvuli kontrolovani spatnemu zapisu
#define UNIT_FLOW_MAX 4
#define UNIT_FLOW_MIN 0
#define UNIT_VOLUME_MAX 3
#define UNIT_VOLUME_MIN 0
#define AVERAGE_SAMPLES_MAX 120
#define AVERAGE_SAMPLES_MIN 1
#define MODBUS_SLAVE_ADDRESS_MAX 247
#define MODBUS_SLAVE_ADDRESS_MIN 1
#define MODBUS_PARITY_MAX 3
#define MODBUS_PARITY_MIN 0
#define FLOW_RANGE_MAX 36000000
#define FLOW_RANGE_MIN 0
#define EXCITATION_FREQUENCY_MAX 1
#define EXCITATION_FREQUENCY_MIN 0

#define REG_PASS_START 2
const unsigned long* RegisterTabPass[]=
{
    &PASSWORD_USER,
    &PASSWORD_SERVICE,
    &PASSWORD_FACTORY,
    &UNKNOWN
};
#define REG_PASS_SIZE ( sizeof(RegisterTabPass)/sizeof(RegisterTabPass[0]) )

#define REG_INFO_START 1000
const unsigned long* RegisterTabInfo[]=
{
    &UNIT_NO,
    &ERRO_MIN,
    &OK_MIN,
    &DIAMETER,
    &FIRM_WARE,
    &FLOW_RANGE,
};
#define REG_INFO_SIZE ( sizeof(RegisterTabInfo)/sizeof(RegisterTabInfo[0]) )

#define REG_REAL_TIME_START 100
const unsigned long* RegisterTabRealTime[]=
{
    &REAL_FLOW,
    &REAL_FLOW_SIGN,
    &REAL_TOTAL_DIG,
    &REAL_TOTAL_DEC,
    &REAL_TOTAL_PLUS_DIG,
    &REAL_TOTAL_PLUS_DEC,
    &REAL_TOTAL_MINUS_DIG,
    &REAL_TOTAL_MINUS_DEC,
    &REAL_TOTAL_AUX_DIG,
    &REAL_TOTAL_AUX_DEC,
    &REAL_ERROR_CODE,
	&ACTUAL_ADDATA,
    &ACTUAL_ADDATAAVG,
    &EMPTY_PIPE_ADC,
    &TEPLOTA_AVG,
    &REAL_TOTAL2_DIG,
    &REAL_TOTAL2_DEC,
    &REAL_TOTAL2_PLUS_DIG,
    &REAL_TOTAL2_PLUS_DEC,
    &REAL_TOTAL2_MINUS_DIG,
    &REAL_TOTAL2_MINUS_DEC,
    &REAL_TOTAL2_AUX_DIG,
    &REAL_TOTAL2_AUX_DEC,
};
#define REG_REAL_TIME_SIZE ( sizeof(RegisterTabRealTime)/sizeof(RegisterTabRealTime[0]) )

#define REG_REAL_TIME_FLOAT_START 150
const unsigned long* RegisterTabRealTimeFloat[]=
{
    &REAL_FLOW_FLOAT,
    &REAL_TOTAL_FLOAT,
    &REAL_TOTAL_PLUS_FLOAT,
    &REAL_TOTAL_MINUS_FLOAT,
    &REAL_TOTAL_AUX_FLOAT
};
#define REG_REAL_TIME_FLOAT_SIZE ( sizeof(RegisterTabRealTimeFloat)/sizeof(RegisterTabRealTimeFloat[0]) )

#define REG_USER_START 2000
const unsigned long* RegisterTabUser[]=
{
    //&PASSWORD_USER,
    &MEASUREMENT_STATE,
    &AIR_DETECTOR,
    &AIR_CONSTANT,
    &AVERAGE_SAMPLES,
    &LOW_FLOW_CUTOFF,
    &INVERT_FLOW,
    &FLOW_RANGE,
    &CLEAN_POWER,
    &DELETE_AUX,
    &MODBUS_SLAVE_ADDRESS,
    &MODBUS_BAUDRATE,
    &MODBUS_PARITY,
    &START_DELAY,
    &DIGIT_FILTER
};
#define REG_USER_SIZE ( sizeof(RegisterTabUser)/sizeof(RegisterTabUser[0]) )

#define REG_SERVICE_START 3000
const unsigned long* RegisterTabService[]=
{
    &DELETE_OK_MIN,
    &DELETE_ERROR_MIN,
    &DELETE_TOTAL_VOLUME,
    &DELETE_NEGATIVE_VOLUME,
    &DELETE_POSITIVE_VOLUME,
    &DEMO,
    &SIMULATED_FLOW,
    &ACTUAL_ADZERO1,
    &ACTUAL_ADZERO2,
    &ACTUAL_ADZERO_DIF,
};
#define REG_SERVICE_SIZE ( sizeof(RegisterTabUser)/sizeof(RegisterTabService[0]) )

#define REG_FACTORY_START 4000
const unsigned long* RegisterTabFactory[]=
{
    &DIAMETER,
    &UNIT_NO,
    &CALIBRATION_POINT_ONE,
    &CALIBRATION_DATA_ONE,
    &CALIBRATION_POINT_TWO,
    &CALIBRATION_DATA_TWO,
    &CALIBRATION_POINT_THREE,
    &CALIBRATION_DATA_THREE,
    &ZERO_FLOW,
    &ZERO_CONSTANT,
    &ZERO_ERASE,
    &EXCITATION_FREQUENCY,
    &EXCITATION,
};
#define REG_FACTORY_SIZE ( sizeof(RegisterTabFactory)/sizeof(RegisterTabFactory[0]) )

#endif
